package renderEngine

import java.io.File
import java.net.URL

import objConverter.OBJFileLoader
import org.scalatest.FunSuite

import scala.collection.mutable

/**
  * Created by Shmuel Blitz on 6/5/2018.
  */
class LoaderTester extends FunSuite {
  private val cubeUri: URL = this.getClass.getResource("/cube.obj")
  test("old Loader and new loader have the same output data") {

    val file = new File(cubeUri.getPath)
    val oldLoaderData = OBJLoader2.loadFileData(file)
    val file2 = new File(cubeUri.getPath)
    val newLoaderData = OBJFileLoader.loadOBJ(file2)

    assert(oldLoaderData !== null)
    assert(newLoaderData !== null)
    assert(oldLoaderData.verticesArray.length !== newLoaderData.getVertices.length)
   // assert(stack.size === oldSize - 1)
  }

  /*test("pop is invoked on an empty stack") {

    val emptyStack = new mutable.Stack[Int]
    intercept[NoSuchElementException] {
      emptyStack.pop()
    }
    assert(emptyStack.isEmpty)
  }*/
}
