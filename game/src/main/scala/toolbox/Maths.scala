package toolbox

import entities.Camera
import org.joml.{Matrix4f, Vector2f, Vector3f}

/**
  * Created by Shmuel Blitz on 5/15/2018.
  */
object Maths {

  import org.joml.Vector2f
  import org.joml.Vector3f

  def barryCentric(p1: Vector3f, p2: Vector3f, p3: Vector3f, pos: Vector2f): Float = {
    val det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z)
    val l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det
    val l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det
    val l3 = 1.0f - l1 - l2
    l1 * p1.y + l2 * p2.y + l3 * p3.y
  }

  import org.joml.Matrix4f
  import org.joml.Vector2f
  import org.joml.Vector3f

  def createTransformationMatrix(translation: Vector2f, scale: Vector2f): Matrix4f = {
    new Matrix4f()
      .identity()
      .translate(translation.x, translation.y, 0)
      .scale(new Vector3f(scale.x, scale.y, 1f))
  }

  def createTransformationMatrix(translation: Vector3f, rx: Float, ry: Float, rz: Float, scale: Float): Matrix4f = {
    new Matrix4f()
      .translate(translation)
      .rotate(Math.toRadians(rx).toFloat, 1.0f, 0.0f, 0.0f)
      .rotate(Math.toRadians(ry).toFloat, 0.0f, 1.0f, 0.0f)
      .rotate(Math.toRadians(rz).toFloat, 0.0f, 0.0f, 1.0f)
      .scale(scale)
  }

  def createViewMatrix(camera: Camera) = {
    new Matrix4f()
      .identity()
      .rotate(Math.toRadians(camera.pitch).toFloat, 1, 0, 0)
      .rotate(Math.toRadians(camera.yaw).toFloat, 0, 1, 0)
      .translate(-camera.position.x, -camera.position.y, -camera.position.z)
  }


}
