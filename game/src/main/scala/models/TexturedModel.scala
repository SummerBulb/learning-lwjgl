package models

import textures.ModelTexture

/**
  * Created by Shmuel Blitz on 5/13/2018.
  */
case class TexturedModel(rawModel: RawModel, texture: ModelTexture ) {

}
