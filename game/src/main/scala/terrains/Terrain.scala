package terrains

import java.awt.image.BufferedImage
import java.io.File

import com.sun.prism.d3d.D3DContext
import javax.imageio.ImageIO
import org.joml.{Vector2f, Vector3f}
import renderEngine.Loader
import textures.{TerrainTexture, TerrainTexturePack}
import toolbox.Maths

/**
  * Created by Shmuel Blitz on 5/27/2018.
  */
class Terrain(gridX: Int, gridZ: Int, val loader: Loader, val texturePack: TerrainTexturePack,
              val blendMap: TerrainTexture, heightMap: String) {

  val SIZE: Float = 800
  val MAX_HEIGHT = 40
  val MIN_HEIGHT = 40
  val MAX_PIXEL_COLOUR = 256f * 256 * 256

  var heights: Array[Array[Float]] = null


  val x = gridX * SIZE
  val z = gridZ * SIZE
  val model = generateTerrain(loader, heightMap)

  def getHeightOfTerrain(worldX: Float, worldZ: Float) = {
    val terrainX = worldX - this.x
    val terrainZ = worldZ - this.z
    val gridSquareSize = SIZE / (heights.length - 1).toFloat
    val gridX = Math.floor(terrainX / gridSquareSize).toInt
    val gridZ = Math.floor(terrainZ / gridSquareSize).toInt
    if (gridX >= heights.length - 1 || gridZ >= heights.length - 1 || gridX < 0 || gridZ < 0) {
      0f
    } else {
      val xCoord = (terrainX % gridSquareSize) / gridSquareSize
      val zCoord = (terrainZ % gridSquareSize) / gridSquareSize

      if (xCoord <= (1 - zCoord)) {
        Maths.barryCentric(
          new Vector3f(0, heights(gridX)(gridZ), 0),
          new Vector3f(1, heights(gridX + 1)(gridZ), 0),
          new Vector3f(0, heights(gridX)(gridZ + 1), 1),
          new Vector2f(xCoord, zCoord)
        )
      } else {
        Maths.barryCentric(
          new Vector3f(1, heights(gridX + 1)(gridZ), 0),
          new Vector3f(1, heights(gridX + 1)(gridZ + 1), 1),
          new Vector3f(0, heights(gridX)(gridZ + 1), 1),
          new Vector2f(xCoord, zCoord)
        )
      }
    }
  }


  private def generateTerrain(loader: Loader, heightMap: String) = {
    val heightMapImage = ImageIO.read(new File(s"game/res/$heightMap.png"))

    val VERTEX_COUNT = heightMapImage.getHeight

    heights = Array.ofDim(VERTEX_COUNT, VERTEX_COUNT)

    val count = VERTEX_COUNT * VERTEX_COUNT
    val vertices = new Array[Float](count * 3)
    val normals = new Array[Float](count * 3)
    val textureCoords = new Array[Float](count * 2)
    val indices = new Array[Int](6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1))
    var vertexPointer = 0
    for {
      i <- 0 until VERTEX_COUNT
      j <- 0 until VERTEX_COUNT
    } {
      vertices(vertexPointer * 3) = j.toFloat / (VERTEX_COUNT.toFloat - 1) * SIZE
      val height = getHeight(j, i, heightMapImage)
      heights(j)(i) = height
      vertices(vertexPointer * 3 + 1) = height
      vertices(vertexPointer * 3 + 2) = i.toFloat / (VERTEX_COUNT.toFloat - 1) * SIZE
      val normal = calculateNormal(j, i, heightMapImage)
      normals(vertexPointer * 3) = normal.x
      normals(vertexPointer * 3 + 1) = normal.y
      normals(vertexPointer * 3 + 2) = normal.z
      textureCoords(vertexPointer * 2) = j.toFloat / (VERTEX_COUNT.toFloat - 1)
      textureCoords(vertexPointer * 2 + 1) = i.toFloat / (VERTEX_COUNT.toFloat - 1)
      vertexPointer += 1
    }
    var pointer = 0
    var gz = 0
    while (gz < VERTEX_COUNT - 1) {
      var gx = 0
      while (gx < VERTEX_COUNT - 1) {
        val topLeft = (gz * VERTEX_COUNT) + gx
        val topRight = topLeft + 1
        val bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx
        val bottomRight = bottomLeft + 1
        indices(pointer) = topLeft
        pointer += 1
        indices(pointer) = bottomLeft
        pointer += 1
        indices(pointer) = topRight
        pointer += 1
        indices(pointer) = topRight
        pointer += 1
        indices(pointer) = bottomLeft
        pointer += 1
        indices(pointer) = bottomRight
        pointer += 1
        gx += 1
      }

      gz += 1
    }


    loader.loadToVAO(vertices, textureCoords, normals, indices)
  }

  def calculateNormal(x: Int, z: Int, heightMap: BufferedImage) = {
    val heightL = getHeight(x - 1, z, heightMap)
    val heightR = getHeight(x + 1, z, heightMap)
    val heightU = getHeight(x, z + 1, heightMap)
    val heightD = getHeight(x, z - 1, heightMap)

    val noraml = new Vector3f(heightL - heightR, 2f, heightD - heightU)
    noraml.normalize()
  }

  def getHeight(x: Int, z: Int, heightMap: BufferedImage) = {
    if (x < 0 || x >= heightMap.getHeight || z < 0 || z >= heightMap.getWidth)
      0f
    else {
      val pixelColour = heightMap.getRGB(x, z)
      val pixelColourShifted = pixelColour + MAX_PIXEL_COLOUR / 2f
      val pixelColourNormalized = pixelColourShifted / (MAX_PIXEL_COLOUR / 2f)
      val height = pixelColourNormalized * MAX_HEIGHT
      height
    }

  }
}
