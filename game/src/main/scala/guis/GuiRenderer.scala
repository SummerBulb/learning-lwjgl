package guis

import org.lwjgl.opengl.{GL11, GL13, GL20, GL30}
import renderEngine.Loader
import toolbox.Maths

/**
  * Created by Shmuel Blitz on 6/17/2018.
  */
class GuiRenderer(loader: Loader) {

  val positions = Array[Float](-1, 1, -1, -1, 1, 1, 1, -1)
  val quad  = loader.loadToVao(positions)
  val shader = new GuiShader

  def render(guis: List[GuiTexture]) = {
    shader.start()
    GL30.glBindVertexArray(quad.vaoId)
    GL20.glEnableVertexAttribArray(0)
    GL11.glEnable(GL11.GL_BLEND)
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
    GL11.glDisable(GL11.GL_DEPTH_TEST)
    guis.foreach((gui: GuiTexture) => {
      GL13.glActiveTexture(GL13.GL_TEXTURE0)
      GL11.glBindTexture(GL11.GL_TEXTURE_2D, gui.texture)
      val matrix = Maths.createTransformationMatrix(gui.position, gui.scale)
      shader.loadTransformation(matrix)
      GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, quad.vertexCount)
    })
    GL11.glEnable(GL11.GL_DEPTH_TEST)
    GL11.glDisable(GL11.GL_BLEND)
    GL20.glDisableVertexAttribArray(0)
    GL30.glBindVertexArray(0)
    shader.stop()
  }

  def cleanUp() = {
    shader.cleanUp()
  }
}
