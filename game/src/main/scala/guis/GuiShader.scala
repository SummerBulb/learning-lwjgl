package guis

import org.joml.Matrix4f
import shaders.ShaderProgram

/**
  * Created by Shmuel Blitz on 6/17/2018.
  */
class GuiShader() extends
  ShaderProgram("game/src/main/scala/guis/guiVertexShader", "game/src/main/scala/guis/guiFragmentShader") {

  val location_transformationMatrix = super.getUniformLocation("transformationMatrix")

  def loadTransformation(matrix: Matrix4f) = {
    super.loadMatrix(location_transformationMatrix, matrix)
  }


  def bindAttributes() = {
    super.bindAttribute(0, "position")
  }

  override protected def getAllUniformLocations(): Unit = {}
}
