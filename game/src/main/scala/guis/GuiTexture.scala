package guis

import org.joml.Vector2f

/**
  * Created by Shmuel Blitz on 6/17/2018.
  */
class GuiTexture(val texture: Int, var position: Vector2f, var scale: Vector2f) {

}
