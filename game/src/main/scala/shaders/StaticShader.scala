package shaders

import java.lang.Math

import entities.{Camera, Light}
import org.joml.{Matrix4f, Vector2f, Vector3f}
import renderEngine.DisplayManager
import toolbox.Maths

/**
  * Created by Shmuel Blitz on 5/13/2018.
  */

class StaticShader()
  extends ShaderProgram("src/main/scala/shaders/vertexShader", "src/main/scala/shaders/fragmentShader") {

  var location_transformationMatrix: Int = super.getUniformLocation("transformationMatrix")
  var location_projectionMatrix: Int = super.getUniformLocation("projectionMatrix")
  var location_viewMatrix: Int = super.getUniformLocation("viewMatrix")
  var location_lightPosition: Int = super.getUniformLocation("lightPosition")
  var location_lightColour: Int = super.getUniformLocation("lightColour")
  var location_shineDamper: Int = super.getUniformLocation("shineDamper")
  var location_reflectivity: Int = super.getUniformLocation("reflectivity")
  var location_usefakeLighting: Int = super.getUniformLocation("useFakeLighting")
  var location_skyColour: Int = super.getUniformLocation("skyColour")
  var location_numberOfRows: Int = super.getUniformLocation("numberOfRows")
  var location_offset: Int = super.getUniformLocation("offset")


  override protected def bindAttributes(): Unit = {
    super.bindAttribute(0, "position")
    super.bindAttribute(1, "textureCoords")
    super.bindAttribute(2, "normal")

  }

  def loadShineVariables(damper: Float, reflectivity: Float) = {
    super.loadFloat(location_shineDamper, damper)
    super.loadFloat(location_reflectivity, reflectivity)
  }

  override protected def getAllUniformLocations(): Unit = {
    // location_transformationMatrix = super.getUniformLocation("transformationMatrix")
  }

  def loadTransformationMatrix(matrix: Matrix4f) = {
    super.loadMatrix(location_transformationMatrix, matrix)
  }

  def loadLight(light: Light): Unit = {
    super.loadVector(location_lightPosition, light.position)
    super.loadVector(location_lightColour, light.colour)
  }

  def loadViewMatrix(camera: Camera) = {
    val viewMatrix = Maths.createViewMatrix(camera)
    super.loadMatrix(location_viewMatrix, viewMatrix)
  }

  def loadProjectionMatrix(projection: Matrix4f) = {
    super.loadMatrix(location_projectionMatrix, projection)
  }

  def loadFakeLighting(useFake:Boolean) = {
    super.loadBoolean(location_usefakeLighting, useFake)
  }

  def loadSkyColour(r:Float, g:Float, b:Float) = {
    super.loadVector(location_skyColour, new Vector3f(r,g,b))
  }

  def loadNumberOfRow(numberOfRows : Int) = {
    super.loadFloat(location_numberOfRows, numberOfRows)
  }

  def loadOffset(x:Float, y:Float) = {
    super.load2DVector(location_offset, new Vector2f(x,y))
  }
}
