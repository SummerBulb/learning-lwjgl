package shaders

import java.nio.FloatBuffer

import org.joml.{Matrix4f, Vector2f, Vector3f}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.{GL11, GL20}

import scala.io.Source._


/**
  * Created by Shmuel Blitz on 5/13/2018.
  */
abstract class ShaderProgram(vertexFile: String, fragmentFile: String) {
//  println("Creating float buffer")
  val matrixBuffer: FloatBuffer = BufferUtils.createFloatBuffer(16)

  val vertexShaderId: Int = loadShader(vertexFile, GL20.GL_VERTEX_SHADER)
  val fragmentShaderId: Int = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER)
  val programId: Int = GL20.glCreateProgram()

  GL20.glAttachShader(programId, vertexShaderId)
  GL20.glAttachShader(programId, fragmentShaderId)
  bindAttributes()
  GL20.glLinkProgram(programId)
  GL20.glValidateProgram(programId)
  getAllUniformLocations()

  protected def getAllUniformLocations(): Unit

  protected def getUniformLocation(uniformName: String) = {
    GL20.glGetUniformLocation(programId, uniformName)
  }

  protected def loadFloat(location: Int, value: Float) = {
    GL20.glUniform1f(location, value)
  }

  protected def loadInt(location: Int, value: Int) = {
    GL20.glUniform1i(location, value)
  }

  protected def loadVector(location: Int, vector: Vector3f) = {
    GL20.glUniform3f(location, vector.x, vector.y, vector.z)
  }

  protected def load2DVector(location: Int, vector: Vector2f) = {
    GL20.glUniform2f(location, vector.x, vector.y)
  }

  protected def loadBoolean(location: Int, value: Boolean) = {
    val toLoad = if (value) 1f else 0
    GL20.glUniform1f(location, toLoad)
  }

  protected def loadMatrix(location: Int, matrix: Matrix4f) = {
    GL20.glUniformMatrix4fv(location, false, matrix.get(matrixBuffer))
  }

  def start() = {
    GL20.glUseProgram(programId)
  }

  def stop() = {
    GL20.glUseProgram(0)
  }

  def cleanUp() = {
    stop()
    GL20.glDetachShader(programId, vertexShaderId)
    GL20.glDetachShader(programId, fragmentShaderId)
    GL20.glDeleteShader(vertexShaderId)
    GL20.glDeleteShader(fragmentShaderId)
    GL20.glDeleteProgram(programId)


  }

  protected def bindAttributes()

  protected def bindAttribute(attribute: Int, variableName: String) = {
    GL20.glBindAttribLocation(programId, attribute, variableName)
  }

  private def loadShader(file: String, shaderType: Int): Int = {
    val source = fromFile(file).getLines.mkString("\n")
    val shaderId = GL20.glCreateShader(shaderType)
    GL20.glShaderSource(shaderId, source)
    GL20.glCompileShader(shaderId)
    if (GL20.glGetShaderi(shaderId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
      println(GL20.glGetShaderInfoLog(shaderId, 500))
      System.err.println(s"Could not compile shader: ${file}")
      System.exit(-1)
    }

    shaderId
  }
}
