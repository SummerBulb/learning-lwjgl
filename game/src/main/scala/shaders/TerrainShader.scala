package shaders

import entities.{Camera, Light}
import org.joml.{Matrix4f, Vector3f}
import toolbox.Maths

/**
  * Created by Shmuel Blitz on 5/27/2018.
  */
class TerrainShader extends
  ShaderProgram("src/main/scala/shaders/terrainVertexShader",
    "src/main/scala/shaders/terrainFragmentShader") {


  var location_transformationMatrix: Int = super.getUniformLocation("transformationMatrix")
  var location_projectionMatrix: Int = super.getUniformLocation("projectionMatrix")
  var location_viewMatrix: Int = super.getUniformLocation("viewMatrix")
  var location_lightPosition: Int = super.getUniformLocation("lightPosition")
  var location_lightColour: Int = super.getUniformLocation("lightColour")
  var location_shineDamper: Int = super.getUniformLocation("shineDamper")
  var location_reflectivity: Int = super.getUniformLocation("reflectivity")
  var location_skyColour: Int = super.getUniformLocation("skyColour")
  var location_backgroundTexture: Int = super.getUniformLocation("backgroundTexture")
  var location_rTexture: Int = super.getUniformLocation("rTexture")
  var location_gTexture: Int = super.getUniformLocation("gTexture")
  var location_bTexture: Int = super.getUniformLocation("bTexture")
  var location_blendMap: Int = super.getUniformLocation("blendMap")


  override protected def bindAttributes(): Unit = {
    super.bindAttribute(0, "position")
    super.bindAttribute(1, "textureCoords")
    super.bindAttribute(2, "normal")

  }

  def loadShineVariables(damper: Float, reflectivity: Float) = {
    super.loadFloat(location_shineDamper, damper)
    super.loadFloat(location_reflectivity, reflectivity)
  }

  override protected def getAllUniformLocations(): Unit = {
    // location_transformationMatrix = super.getUniformLocation("transformationMatrix")
  }

  def loadTransformationMatrix(matrix: Matrix4f) = {
    super.loadMatrix(location_transformationMatrix, matrix)
  }

  def loadLight(light: Light): Unit = {
    super.loadVector(location_lightPosition, light.position)
    super.loadVector(location_lightColour, light.colour)
  }

  def loadViewMatrix(camera: Camera) = {
    val viewMatrix = Maths.createViewMatrix(camera)
    super.loadMatrix(location_viewMatrix, viewMatrix)
  }

  def loadProjectionMatrix(projection: Matrix4f) = {
    super.loadMatrix(location_projectionMatrix, projection)
  }

  def loadSkyColour(r:Float, g:Float, b:Float) = {
    super.loadVector(location_skyColour, new Vector3f(r,g,b))
  }

  def connectTectureUnits() = {
    super.loadInt(location_backgroundTexture, 0)
    super.loadInt(location_rTexture, 1)
    super.loadInt(location_gTexture, 2)
    super.loadInt(location_bTexture, 3)
    super.loadInt(location_blendMap, 4)
  }


}
