package renderEngine

import entities.{Camera, Entity, Light}
import models.TexturedModel
import org.joml.{Matrix4f, Vector3f}
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL11.{glClear, glClearColor}
import renderEngine.MasterRenderer.enableCulling
import shaders.{StaticShader, TerrainShader}
import terrains.Terrain

import scala.collection.mutable

/**
  * Created by Shmuel Blitz on 5/27/2018.
  */
class MasterRenderer(FOV: Float = 70, NEAR_PLANE: Float = 0.1f, FAR_PLANE: Float = 1000) {

  enableCulling()
  private val projectionMatrix: Matrix4f = createProjectionMatrix()
  private val shader = new StaticShader()
  private val renderer = new EntityRenderer(shader, projectionMatrix)
  private val terrainShader = new TerrainShader()
  private val terrainRenderer = new TerrainRendered(terrainShader, projectionMatrix)


  private val entities = mutable.Map[TexturedModel, List[Entity]]()
  private val terrains = mutable.MutableList[Terrain]()

  val RED = 0.5f
  val GREEN = 0.5f
  val BLUE = 0.5f


  def render(sun: Light, camera: Camera) = {
    prepare()
    shader.start()
    shader.loadSkyColour(RED, GREEN, BLUE)
    shader.loadLight(sun)
    shader.loadViewMatrix(camera)
    renderer.render(entities.toMap)
    shader.stop()
    terrainShader.start()
    terrainShader.loadSkyColour(RED, GREEN, BLUE)
    terrainShader.loadLight(sun)
    terrainShader.loadViewMatrix(camera)
    terrainRenderer.render(terrains)
    terrainShader.stop()
    entities.clear()
    terrains.clear()
  }

  def processTerrain(terrain: Terrain) = {
    terrains += terrain
  }

  def processEntity(entity: Entity) = {
    entities.get(entity.model) match {
      case None => entities += entity.model -> List(entity)
      case Some(lst) => entities(entity.model) = lst :+ entity
    }

  }

  def cleanUp() = {
    shader.cleanUp()
    terrainShader.cleanUp()
  }

  def prepare() = {
    GL11.glEnable(GL11.GL_DEPTH_TEST)
    glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // clear the framebuffer
    glClearColor(RED, GREEN, BLUE, 1f)

  }


  def createProjectionMatrix(): Matrix4f = {
    val w = BufferUtils.createIntBuffer(4)
    val h = BufferUtils.createIntBuffer(4)
    GLFW.glfwGetWindowSize(DisplayManager.window, w, h)
    val width = w.get(0)
    val height = h.get(0)
    val aspectRation = width.toFloat / height.toFloat
    val yScale: Float = (1f / (FOV.toFloat / 2).toRadians) * aspectRation
    val xScale = yScale / aspectRation
    val frustumLength = FAR_PLANE - NEAR_PLANE

    new Matrix4f()
      ._m00(xScale)
      .m11(yScale)
      .m22(-((FAR_PLANE + NEAR_PLANE) / frustumLength))
      .m23(-1)
      .m32(-((2 * NEAR_PLANE * FAR_PLANE) / frustumLength))
      .m33(0)
  }

}

object MasterRenderer {
  def enableCulling(): Unit = {
    GL11.glEnable(GL11.GL_CULL_FACE)
    GL11.glCullFace(GL11.GL_BACK)
  }

  def disableCulling(): Unit = {
    GL11.glDisable(GL11.GL_CULL_FACE)
  }
}