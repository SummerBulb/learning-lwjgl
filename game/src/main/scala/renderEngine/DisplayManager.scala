package renderEngine

import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw._
import org.lwjgl.opengl._


/**
  * Created by Shmuel Blitz on 5/12/2018.
  */
object DisplayManager {


  private val errorCallback = GLFWErrorCallback.createPrint(System.err)

  val WINDOW_WIDTH: Int = 1280
  val WINDOW_HEIGHT: Int = 720

  var lastFrameTime: Long = getCurrentTime()
  var delta: Float = 0f;

  var window = 0L
  val kbInput = new KeyboardInput
  val cursorPositionInput = new CursorPositionInput
  val mouseButtonInput = new MouseButtonInput
  var mouseScrollInput = new MouseScrollInput

  def createDisplay() = {
    glfwInit()
    glfwSetErrorCallback(errorCallback)

    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE)
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "My First Game - LWJGL3", 0, 0)

    val videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor())
    glfwSetWindowPos(window, (videoMode.width - WINDOW_WIDTH) / 2, (videoMode.height - WINDOW_HEIGHT) / 2)

    glfwSetKeyCallback(window, kbInput)
    glfwSetCursorPosCallback(window, cursorPositionInput)
    glfwSetMouseButtonCallback(window, mouseButtonInput)
    glfwSetScrollCallback(window, mouseScrollInput)


    // Make the OpenGL context current
    glfwMakeContextCurrent(window);
    // Enable v-sync
    glfwSwapInterval(1)

    // Make the window visible
    glfwShowWindow(window)
    GL.createCapabilities

  }

  def frameTimeSeconds = delta

  def updateDisplay() = {
    // swap the color buffers
    glfwSwapBuffers(window);
    // Poll for window events. The key callback above will only be
    // invoked during this call.
    cursorPositionInput.clearDelta()
    mouseScrollInput.clear()
    glfwPollEvents();

    val currentFrameTime = getCurrentTime()
    delta = (currentFrameTime - lastFrameTime) / 1000f
    lastFrameTime = currentFrameTime


    if (kbInput.keys(GLFW_KEY_F1))
      println(s"fps: ${1 / delta}")

  }

  def closeDisplay() = {
    GLFW.glfwTerminate()
  }

  def getCurrentTime() = {
    System.nanoTime() / (1000 * 1000)
  }
}
