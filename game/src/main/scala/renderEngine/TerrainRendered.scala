package renderEngine

import entities.Entity
import models.TexturedModel
import org.joml.{Matrix4f, Vector3f}
import org.lwjgl.opengl.{GL11, GL13, GL20, GL30}
import shaders.TerrainShader
import terrains.Terrain
import toolbox.Maths

import scala.collection.mutable

/**
  * Created by Shmuel Blitz on 5/27/2018.
  */
class TerrainRendered(shader: TerrainShader, projectionMatrix: Matrix4f) {

  shader.start()
  shader.loadProjectionMatrix(projectionMatrix)
  shader.connectTectureUnits()
  shader.stop()

  def render(terrains: mutable.MutableList[Terrain]) = {
    terrains.foreach((terrain: Terrain) =>{
      prepareTerain(terrain)
      loadModelMatrix(terrain)
      //render
      GL11.glDrawElements(GL11.GL_TRIANGLES, terrain.model.vertexCount, GL11.GL_UNSIGNED_INT, 0)

      unbindTexturedModel()
    })

  }


  private def prepareTerain(terrain: Terrain) = {
    val rawModel = terrain.model
    GL30.glBindVertexArray(rawModel.vaoId)
    GL20.glEnableVertexAttribArray(0)
    GL20.glEnableVertexAttribArray(1)
    GL20.glEnableVertexAttribArray(2)
    bindTextures(terrain)
    shader.loadShineVariables(1,0)

  }

  private def bindTextures(terrain: Terrain) = {
    val texturePack = terrain.texturePack
    GL13.glActiveTexture(GL13.GL_TEXTURE0)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.backgroundTexture.textureId)
    GL13.glActiveTexture(GL13.GL_TEXTURE1)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.rTexture.textureId)
    GL13.glActiveTexture(GL13.GL_TEXTURE2)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.gTexture.textureId)
    GL13.glActiveTexture(GL13.GL_TEXTURE3)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.bTexture.textureId)
    GL13.glActiveTexture(GL13.GL_TEXTURE4)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, terrain.blendMap.textureId)

  }

  private def unbindTexturedModel() = {
    GL20.glDisableVertexAttribArray(0)
    GL20.glDisableVertexAttribArray(1)
    GL20.glDisableVertexAttribArray(2)
    GL30.glBindVertexArray(0)
  }

  private def loadModelMatrix(terrain: Terrain) = {
    val transformationMatrix = Maths.createTransformationMatrix(
      new Vector3f(terrain.x, 0, terrain.z), 0, 0, 0, 1)
    shader.loadTransformationMatrix(transformationMatrix)
  }

}
