package renderEngine

import org.lwjgl.glfw.{GLFW, GLFWCursorPosCallback}

/**
  * Created by Shmuel Blitz on 5/16/2018.
  */
class CursorPositionInput extends GLFWCursorPosCallback{
  var xPos = 0f
  var yPos = 0f
  var dx = 0f
  var dy = 0f

  override def invoke(window: Long, newX: Double, newY: Double): Unit = {
    dx = newX.toFloat - xPos
    dy = newY.toFloat - yPos
    xPos = newX.toFloat
    yPos = newY.toFloat
  }

  def clearDelta() = {
    dx = 0
    dy = 0
  }
}
