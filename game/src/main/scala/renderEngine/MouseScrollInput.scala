package renderEngine

import org.lwjgl.glfw.{GLFW, GLFWScrollCallback}

/**
  * Created by Shmuel Blitz on 5/16/2018.
  */
class MouseScrollInput extends GLFWScrollCallback {

  var xOffset = 0f
  var yOffset = 0f

  override def invoke(window: Long, xoffset: Double, yoffset: Double): Unit = {
    xOffset = xoffset.toFloat
    yOffset = yoffset.toFloat
  }

  def clear() = {
    xOffset = 0
    yOffset = 0
  }
}
