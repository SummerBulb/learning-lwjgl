package renderEngine

import java.nio.{FloatBuffer, IntBuffer}

import models.RawModel
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.{GL_LINEAR_MIPMAP_LINEAR, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER}
import org.lwjgl.opengl._
import utils.Texture

import scala.collection.mutable

/**
  * Created by Shmuel Blitz on 5/13/2018.
  */
class Loader {

  val vaos = mutable.MutableList[Int]()
  val vbos = mutable.MutableList[Int]()
  val textures = mutable.MutableList[Int]()

  def loadToVAO(positions: Array[Float], textureCoords: Array[Float], normals: Array[Float], indices: Array[Int]): RawModel = {
    val vaoId = createVAO()
    bindIndicesBuffer(indices)
    storeDataInAttributeList(0, 3, positions)
    storeDataInAttributeList(1, 2, textureCoords)
    storeDataInAttributeList(2, 3, normals)
    unbindVAO()
    new RawModel(vaoId, indices.length)
  }

  def loadToVao(positions: Array[Float]) = {
    val vaoId = createVAO()
    storeDataInAttributeList(0, 2, positions)
    unbindVAO()
    new RawModel(vaoId, positions.length / 2)
  }

  def loadTexture(filename: String) = {
    GL11.glGenTextures()
    val texture = Texture.loadTexture(s"res/$filename.png")
    GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D)
    GL11.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
    GL11.glTexParameterf(GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.4f)

    val textureId = texture.getId
    textures += textureId
    textureId
  }

  def cleanUp() = {
    vaos.foreach(vao => GL30.glDeleteVertexArrays(vao))
    vbos.foreach(vbo => GL15.glDeleteBuffers(vbo))
    textures.foreach(texture => GL11.glDeleteTextures(texture))
  }

  private def createVAO(): Int = {
    val vaoId = GL30.glGenVertexArrays()
    vaos += vaoId
    GL30.glBindVertexArray(vaoId)
    vaoId
  }

  private def storeDataInAttributeList(attributeNumber: Int, coordinateSize: Int, data: Array[Float]): Unit = {
    val vboId = GL15.glGenBuffers()
    vbos += vboId
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
    val buffer = storeDataInFloatBuffer(data)
    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW)
    GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0, 0)
    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)

  }

  private def unbindVAO(): Unit = {
    GL30.glBindVertexArray(0)
  }

  private def bindIndicesBuffer(indices: Array[Int]) = {
    val vboId = GL15.glGenBuffers()
    vbos += vboId
    GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboId)
    val buffer = storDataInIntBuffer(indices)
    GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW)
  }

  private def storDataInIntBuffer(data: Array[Int]): IntBuffer = {
    val buffer = BufferUtils.createIntBuffer(data.length)
    buffer.put(data)
    buffer.flip()
    buffer
  }

  private def storeDataInFloatBuffer(data: Array[Float]): FloatBuffer = {
    val buffer = BufferUtils.createFloatBuffer(data.length)
    buffer.put(data)
    buffer.flip()
    buffer
  }
}
