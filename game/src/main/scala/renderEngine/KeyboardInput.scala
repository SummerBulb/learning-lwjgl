package renderEngine

import org.lwjgl.glfw.{GLFW, GLFWKeyCallback}

/**
  * Created by Shmuel Blitz on 5/16/2018.
  */
class KeyboardInput extends GLFWKeyCallback{

  val keys = new Array[Boolean](500)

  override def invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit = {
    keys(key) = (action != GLFW.GLFW_RELEASE)
  }
}
