package renderEngine

import java.io.File

import models.RawModel
import org.apache.log4j.Logger
import org.apache.log4j.spi.LoggerFactory
import org.joml.{Vector2f, Vector3f}

import scala.collection.mutable
import scala.io.Source

/**
  * Created by Shmuel Blitz on 5/18/2018.
  */
case class LoadFileData(val indices: mutable.MutableList[Int], val texturesArray: Array[Float], val normalsArray: Array[Float], val verticesArray: Array[Float])

/**
  * This is an old version, kept only for future work. you should use <code>OBJFileLoder</code> instead.
  * */
@Deprecated
object OBJLoader2 {

  org.apache.log4j.BasicConfigurator.configure()
  val logger = Logger.getLogger(OBJLoader.getClass)


  def loadOBJModel(fileName: String, loader: Loader): RawModel = {
    val data = loadFileData(fileName)
    loadToVao(loader, data.indices, data.texturesArray, data.normalsArray, data.verticesArray)
  }


  def loadFileData(fileName: String): LoadFileData = {
    loadFileData(new File(s"game/res/$fileName.obj"))
  }

  def loadFileData(file: File): LoadFileData = {
    val bufferedSource = Source.fromFile(file)
    val lines = bufferedSource.getLines()

    var line = ""
    var vertices = mutable.ArrayBuffer[Vector3f]()
    var textures = mutable.ArrayBuffer[Vector2f]()
    var normals = mutable.ArrayBuffer[Vector3f]()
    var indices = mutable.MutableList[Int]()
    var texturesToArray: Array[Vector2f] = null
    var normalsToArray: Array[Vector3f] = null
    var texturesArray: Array[Float] = null
    var normalsArray: Array[Float] = null
    //var indicesArray: Array[Int] = null


    var firstPhase = true
    logger.debug("Reading file...")
    for (line <- lines) {
      if (firstPhase) {
        val currentLine = line.split(" ")
        if (currentLine(0) == "v") {
          val vertex = new Vector3f(currentLine(1).toFloat, currentLine(2).toFloat, currentLine(3).toFloat)
          vertices += vertex
        } else if (currentLine(0) == "vt") {
          val texture = new Vector2f(currentLine(1).toFloat, currentLine(2).toFloat)
          textures += texture
        } else if (currentLine(0) == "vn") {
          val normal = new Vector3f(currentLine(1).toFloat, currentLine(2).toFloat, currentLine(3).toFloat)
          normals += normal
        } else if (currentLine(0) == "f") {
          texturesArray = new Array[Float](vertices.size * 2)
          normalsArray = new Array[Float](vertices.size * 3)
          texturesToArray = textures.toArray
          normalsToArray = normals.toArray
          firstPhase = false
        }
      }

      if (!firstPhase) {
        if (line.startsWith("f ")) {
          val currentLine = line.split(" ")
          val vertex1 = currentLine(1).split("/")
          val vertex2 = currentLine(2).split("/")
          val vertex3 = currentLine(3).split("/")

          processVertex(vertex1, indices, texturesToArray, normalsToArray, texturesArray, normalsArray)
          processVertex(vertex2, indices, texturesToArray, normalsToArray, texturesArray, normalsArray)
          processVertex(vertex3, indices, texturesToArray, normalsToArray, texturesArray, normalsArray)
        }
      }
    }

    bufferedSource.close()
    logger.debug("Finished reading file.")

    val verticesArray = vertices.flatMap((v: Vector3f) => Seq(v.x, v.y, v.z)).toArray
    new LoadFileData(indices, texturesArray, normalsArray, verticesArray)
  }

  private def loadToVao(loader: Loader, indices: mutable.MutableList[Int], texturesArray: Array[Float],
                        normalsArray: Array[Float], verticesArray: Array[Float]): RawModel = {
    logger.debug("Loading to VAO")
    val model = loader.loadToVAO(verticesArray, texturesArray, normalsArray, indices.toArray)
    logger.debug("Done loading to VAO")
    model
  }

  def processVertex(vertexData: Array[String], indices: mutable.MutableList[Int],
                    textures: Array[Vector2f], normals: Array[Vector3f],
                    texturesArray: Array[Float], normalsArray: Array[Float]): Unit = {
    val currentVertexPointer = vertexData(0).toInt - 1
    indices += currentVertexPointer
    val currentTex = textures(vertexData(1).toInt - 1)
    texturesArray(currentVertexPointer * 2) = currentTex.x
    texturesArray(currentVertexPointer * 2 + 1) = 1 - currentTex.y
    val currentNorm = normals(vertexData(2).toInt - 1)
    normalsArray(currentVertexPointer * 3) = currentNorm.x
    normalsArray(currentVertexPointer * 3 + 1) = currentNorm.y
    normalsArray(currentVertexPointer * 3 + 2) = currentNorm.z
  }

}
