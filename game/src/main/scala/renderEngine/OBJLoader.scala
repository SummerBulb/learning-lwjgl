package renderEngine

import models.RawModel
import org.joml.{Vector2f, Vector3f}

import scala.collection.mutable
import scala.io.Source

/**
  * Created by Shmuel Blitz on 5/17/2018.
  */
object OBJLoader {


  def loadOBJModel(fileName: String, loader: Loader): RawModel = {
    val bufferedSource = Source.fromFile(s"game/res/$fileName.obj")
    val objLines = bufferedSource.getLines.toStream.map(line => {
      val split = line.split(" ")
      (split.head, split.tail)
    }).groupBy(_._1)

    val vertices = objLines("v").map((tuple: (String, Array[String])) => {
      val split = tuple._2
      new Vector3f(split(0).toFloat, split(1).toFloat, split(2).toFloat)
    })

    val normals = objLines("vn").map((tuple: (String, Array[String])) => {
      val split = tuple._2
      new Vector3f(split(0).toFloat, split(1).toFloat, split(2).toFloat)
    })

    val textures = objLines("vt").map((tuple: (String, Array[String])) => {
      val split = tuple._2
      new Vector2f(split(0).toFloat, split(1).toFloat)
    })

    val indexTriplets = objLines("f").flatMap((tuple: (String, Array[String])) => {
      tuple._2.toSeq.map((triplet: String) => {
        val vData = triplet.split("/").map(_.toInt)
        (vData(0), vData(1), vData(2))
      })
    })

    val objVectors = indexTriplets
      .map((tuple: (Int, Int, Int)) => {
        val index = tuple._1 - 1
        val texture = textures(tuple._2 - 1)
        val texturesSeq = Seq((index * 2, texture.x), (index * 2 + 1, 1 - texture.y))
        val normal = normals(tuple._3 - 1)
        val normalsSeq = Seq((index * 3, normal.x), (index * 3 + 1, normal.y), (index * 3 + 2, normal.z))
        (index, texturesSeq, normalsSeq)
      })
    val (indices, texturesSeq, normalsSeq) = objVectors.unzip3

    val verticesArray = vertices.flatMap((v: Vector3f) => Seq(v.x, v.y, v.z)).toArray
    val indicesArray = indices.toArray
    val texturesArray = texturesSeq.flatten.sortBy(_._1).map(_._2).toArray
    val normalsArray = normalsSeq.flatten.sortBy(_._1).map(_._2).toArray


    bufferedSource.close

    loader.loadToVAO(verticesArray, texturesArray, normalsArray,indicesArray)
  }




}
