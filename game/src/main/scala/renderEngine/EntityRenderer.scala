package renderEngine

import entities.Entity
import models.TexturedModel
import org.joml.Matrix4f
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL11.{GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, glClear, glClearColor}
import org.lwjgl.opengl._
import shaders.StaticShader
import toolbox.Maths

/**
  * Created by Shmuel Blitz on 5/13/2018.
  */
class EntityRenderer(shader: StaticShader, projectionMatrix: Matrix4f) {

  shader.start()
  shader.loadProjectionMatrix(projectionMatrix)
  shader.stop()


  def render(entities: Map[TexturedModel, List[Entity]]) = {
    entities.foreach { case (model: TexturedModel, batch: List[Entity]) => {
      prepareTexturedModel(model)
      batch.foreach((entity: Entity) => {
        prepareInstance(entity)
        // Do render stuff
        GL11.glDrawElements(GL11.GL_TRIANGLES, model.rawModel.vertexCount, GL11.GL_UNSIGNED_INT, 0)

      })
      unbindTexturedModel()
    }
    }
  }

  private def prepareTexturedModel(model: TexturedModel) = {
    val rawModel = model.rawModel
    GL30.glBindVertexArray(rawModel.vaoId)
    GL20.glEnableVertexAttribArray(0)
    GL20.glEnableVertexAttribArray(1)
    GL20.glEnableVertexAttribArray(2)
    val texture = model.texture
    shader.loadNumberOfRow(texture.numberOfRows)
    if (texture.hasTransparency) {
      MasterRenderer.disableCulling()
    }
    shader.loadFakeLighting(texture.useFakeLighting)
    shader.loadShineVariables(texture.shineDamper, texture.reflectivity)
    GL13.glActiveTexture(GL13.GL_TEXTURE0)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.texture.id)

  }

  private def unbindTexturedModel() = {
    MasterRenderer.enableCulling()
    GL20.glDisableVertexAttribArray(0)
    GL20.glDisableVertexAttribArray(1)
    GL20.glDisableVertexAttribArray(2)
    GL30.glBindVertexArray(0)
  }

  private def prepareInstance(entity: Entity) = {
    val transformationMatrix = Maths.createTransformationMatrix(entity.position,
      entity.rotX, entity.rotY, entity.rotZ, entity.scale)
    shader.loadTransformationMatrix(transformationMatrix)
    shader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset())
  }


}
