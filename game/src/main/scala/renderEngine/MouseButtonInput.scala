package renderEngine

import org.lwjgl.glfw.{GLFW, GLFWCursorPosCallback, GLFWMouseButtonCallback}

/**
  * Created by Shmuel Blitz on 5/16/2018.
  */
class MouseButtonInput extends GLFWMouseButtonCallback {

  val buttons = Array.fill(7)(false)

  override def invoke(window: Long, button: Int, action: Int, mods: Int): Unit = {
    buttons(button) = (action == GLFW.GLFW_PRESS)
  }
}
