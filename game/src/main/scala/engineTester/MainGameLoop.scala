package engineTester

import entities._
import guis.{GuiRenderer, GuiTexture}
import models.TexturedModel
import objConverter.OBJFileLoader
import org.joml.{Vector2f, Vector3f}
import org.lwjgl.glfw.GLFW
import renderEngine._
import terrains.Terrain
import textures.{ModelTexture, TerrainTexture, TerrainTexturePack}

import scala.util.Random

/**
  * Created by Shmuel Blitz on 5/12/2018.
  */
object MainGameLoop {


  def main(args: Array[String]): Unit = {

    DisplayManager.createDisplay()
    val loader = new Loader()

    val backgroundTexture = TerrainTexture(loader.loadTexture("grassy2"))
    val rTexture = TerrainTexture(loader.loadTexture("mud"))
    val gTexture = TerrainTexture(loader.loadTexture("grassFlowers"))
    val bTexture = TerrainTexture(loader.loadTexture("path"))

    val texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture)
    val blendMap = TerrainTexture(loader.loadTexture("blendMap"))

    val terrain: Terrain = new Terrain(0, 0, loader, texturePack, blendMap, "heightmap")
    //    val terrain2: Terrain = new Terrain(1, 0, loader, texturePack, blendMap, "heightmap")

    val trees = (1 to 100) map { _ =>
      val xPos = Random.nextFloat() * 1000
      val zPos = Random.nextFloat() * 500
      Entity(TexturedModel(loadToVao("tree", loader), ModelTexture(loader.loadTexture("tree"), 1, 0)), new Vector3f(
        xPos,
        terrain.getHeightOfTerrain(xPos, zPos),
        zPos),
        0, 0, 0, 8)
    }

    val lowPolyTrees = (1 to 50) map { _ =>
      val xPos = Random.nextFloat() * 1000
      val zPos = Random.nextFloat() * 500
      Entity(TexturedModel(loadToVao("lowPolyTree", loader), ModelTexture(loader.loadTexture("lowPolyTree"), 1, 0, numberOfRows = 2)), new Vector3f(
        xPos,
        terrain.getHeightOfTerrain(xPos, zPos),
        zPos),
        0, 0, 0, 1, Random.nextInt(4))
    }


    val powerups = (1 to 50) map { _ =>
      val xPos = Random.nextFloat() * 1000
      val zPos = Random.nextFloat() * 500
      Entity(TexturedModel(loadToVao("lightning", loader), ModelTexture(loader.loadTexture("yellow"), 20, 3)), new Vector3f(
        xPos,
        terrain.getHeightOfTerrain(xPos, zPos),
        zPos),
        0, 0, 0, 1)
    }

    //    val grassModel = TexturedModel(loadToVao("grassModel", loader), ModelTexture(loader.loadTexture("grassTexture"), 1, 0, true, true))
    //    val grassEntities = spreadRandomly(grassModel, terrain)

    val fernModel = TexturedModel(loadToVao("fern", loader), ModelTexture(loader.loadTexture("fern"), 1, 0, true, false, 2))
    val fernEntities = spreadRandomly(fernModel, terrain)

    val light = new Light(new Vector3f(800, 1000, 300), new Vector3f(1, 1, 1))
    val lightCube = TexturedModel(loadToVao("cube", loader), ModelTexture(loader.loadTexture("white"), 1, 100))
    val lightEntity = Entity(lightCube, light.position, 0, 0, 0, 1)
    lightEntity.position.y += 2

    val powerBar = new GuiTexture(loader.loadTexture("white"), new Vector2f(-0.75f, -0.90f), new Vector2f(0.20f, 0.03f))

    val snakeModels = TexturedModel(loadToVao("ball", loader), ModelTexture(loader.loadTexture("light-brown"), 1, 0)) ::
      TexturedModel(loadToVao("ball", loader), ModelTexture(loader.loadTexture("dark-brown"), 1, 0)) :: Nil
    val player = new SnakePlayer(snakeModels, new Vector3f(150, 0, 200), 0, 0, 0, 1, powerBar, powerups)
    //    val playerModel = new TexturedModel(loadToVao("person", loader), ModelTexture(loader.loadTexture("playerTexture"), 1, 0))
    //    val player = new FigurePlayer(playerModel, new Vector3f(150, 0, 200), 0, 0, 0, 1, powerBar, powerups)

    val camera = new Camera(player, terrain)
    val cameraStartPosition = new Vector3f(120, 100f, 15f)
    camera.position.set(cameraStartPosition)
    camera.yaw = 180


    val guis = List[GuiTexture](
      new GuiTexture(loader.loadTexture("blue"), new Vector2f(-0.75f, -0.90f), new Vector2f(0.22f, 0.06f)),
      powerBar
    )

    val gameOver = new GuiTexture(loader.loadTexture("gameOver"), new Vector2f(0f, 0f), new Vector2f(0.5f, 0.5f))


    val guiRenderer = new GuiRenderer(loader)


    val renderer = new MasterRenderer()
    while (!GLFW.glfwWindowShouldClose(DisplayManager.window)) {

      camera.move()
      if (player.isAlive)
        player.move(terrain)

      powerups.foreach(powerup => powerup.increaseRotation(0, 1, 0))

      player.entities foreach renderer.processEntity
      renderer.processEntity(lightEntity)
      renderer.processTerrain(terrain)
      //      renderer.processTerrain(terrain2)
      trees foreach renderer.processEntity
      lowPolyTrees foreach renderer.processEntity
      powerups foreach renderer.processEntity
      //grassEntities foreach renderer.processEntity
      fernEntities foreach renderer.processEntity

      renderer.render(light, camera)
      guiRenderer.render(guis)
      if (!player.isAlive) {
        guiRenderer.render(gameOver :: Nil)
      }
      DisplayManager.updateDisplay()
    }

    renderer.cleanUp()
    guiRenderer.cleanUp()
    loader.cleanUp()
    DisplayManager.closeDisplay()
  }


  private def spreadRandomly(fernModel: TexturedModel, terrain: Terrain) = {
    (1 to 200) map { _ =>
      val xPos = Random.nextFloat() * 1000
      val zPos = Random.nextFloat() * 500
      Entity(fernModel, new Vector3f(
        xPos,
        terrain.getHeightOfTerrain(xPos, zPos),
        zPos),
        0, 0, 0, 1, Random.nextInt(4))
    }
  }

  private def loadToVao(fileName: String, loader: Loader) = {
    val data = OBJFileLoader.loadOBJ(fileName)
    loader.loadToVAO(data.getVertices, data.getTextureCoords, data.getNormals, data.getIndices)
  }
}
