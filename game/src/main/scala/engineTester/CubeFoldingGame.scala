package engineTester

import engineTester.cube.Solver
import entities._
import examples.cube.{FoldCreature, Runner => CubeRunner}
import genetic.Creature
import models.TexturedModel
import objConverter.OBJFileLoader
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW
import renderEngine._
import textures.ModelTexture

import scala.collection.immutable

/**
  * Created by Shmuel Blitz on 5/12/2018.
  */
object CubeFoldingGame {


  def main(args: Array[String]): Unit = {

    //Thread.sleep(20000)

    DisplayManager.createDisplay()
    val kbInput = DisplayManager.kbInput
    val loader = new Loader()

    //    val model = loadToVao("dragon", loader)
    //    val texture = ModelTexture(loader.loadTexture("blue"), 10, 1)
    //    val model = OBJLoader2.loadOBJModel("tree", loader)
    //    val texture = ModelTexture(loader.loadTexture("tree"), 1, 0)
    //    val model = OBJLoader2.loadOBJModel("stall", loader)
    //    val texture = ModelTexture(loader.loadTexture("stallTexture"), 10, 1)
    //    val texturedModel = TexturedModel(model, texture)

    val snake = CubeRunner.cubeSnake

    val cubeSolution = Array.ofDim[Int](64, 64, 64)
    var x, y, z = 0

    cubeSolution(0)(0)(0) = 1
    snake.zipWithIndex.foreach {
      case (cnt: Int, idx: Int) => {
        1 to cnt foreach { _ => {
          if (idx % 2 != 0) x += 1 else z += 1
          cubeSolution(x)(0)(z) = 1
        }
        }
      }
    }

    val simpleCube = Array.ofDim[Int](8, 8, 8)
    simpleCube(0)(0)(0) = 1
    simpleCube(0)(0)(1) = 1
    simpleCube(0)(0)(2) = 1
    simpleCube(0)(0)(3) = 1
    simpleCube(1)(0)(3) = 1
    simpleCube(2)(0)(3) = 1
    simpleCube(3)(0)(3) = 1
    simpleCube(4)(0)(3) = 1

    //    val cubes: immutable.IndexedSeq[Entity] = cubeMatrixToEntities(loader, cubeSolution, new Vector3f(0, 0, 0))
    val cubes: immutable.IndexedSeq[Entity] = cubeMatrixToEntities(loader, simpleCube, new Vector3f(0, 0, 0))

    val cubeModel = new TexturedModel(loadToVao("cube", loader), ModelTexture(loader.loadTexture("blue"), 10, 0.2f))
    val startPosition = new Vector3f(0f, 1.5f, 0f)
    val selectorEntity = Entity(cubeModel, startPosition, 0, 0, 0, 0.4f)
    val snakeBender = new SnakeBender(selectorEntity, 0, kbInput, cubes)




    //        val entity = Entity(texturedModel, new Vector3f(-10, 0, 25), 0, 180, 0, 1)
    //        val entity2 = Entity(texturedModel, new Vector3f(10, 0, 25), 0, 0, 0, 1)

    //val cubeSolver = new CubeRunner()
    //    val cubeResult = cubeSolver.runExample()
    //    val maybeCube = cubeSolver.toCube(cubeResult._2.foldDirections)
    //    val solutionEntities = cubeMatrixToEntities(loader, maybeCube.get, new Vector3f(0, 0, -15))
    var generationBestFold: FoldCreature = null
    var bestCubeEntities: Seq[Entity] = null


    def updateBestCubeFold(creature: FoldCreature) = {
      generationBestFold = creature
    }

    val solver = new CubeRunner(updateBestCubeFold)
    //val solverThread = new Thread(solver)
    //solverThread.start()


    def bestFoldEntities(fold : FoldCreature) = {
      val maybeCube = solver.toCube(fold.foldDirections)
      cubeMatrixToEntities(loader, maybeCube.get, new Vector3f(0, 0, -15))
    }

    val light = new Light(new Vector3f(100, 200, -100), new Vector3f(1, 1, 1))

    //    val terrain: Terrain = new Terrain(0, 0, loader, new ModelTexture(loader.loadTexture("grass")))
    //    val terrain2: Terrain = new Terrain(1, 0, loader, new ModelTexture(loader.loadTexture("image")))


    val camera = new Camera(snakeBender, null)
    camera.position.set(-8, 6.5f, 25)
    camera.pitch = 21
    camera.yaw = 43

    var selectorPos = 0

    val renderer = new MasterRenderer()


    solver.init()
    while (!GLFW.glfwWindowShouldClose(DisplayManager.window) && !kbInput.keys(GLFW.GLFW_KEY_ESCAPE)) {

      if (kbInput.keys(GLFW.GLFW_KEY_F8))
        println(camera)
      //      entity.increaseRotation(0, 1, 0)
      //      entity2.increaseRotation(0, -2, 0)
      //boxEntity.increaseRotation(0, 1, 0)
      camera.move()
     // if (kbInput.keys(GLFW.GLFW_KEY_SPACE)) {
      if (!solver.goalSatisfied()) {
        solver.step()
      }

      snakeBender.moveSelector()
      snakeBender.moveSelected()

      //      renderer.processTerrain(terrain)
      //      renderer.processTerrain(terrain2)
      cubes.foreach(renderer.processEntity)
      renderer.processEntity(snakeBender.entity)
//      bestFoldEntities(generationBestFold).foreach(renderer.processEntity)
//      bestFoldEntities(solver.weakestCreature()).foreach(renderer.processEntity)
      bestFoldEntities(solver.strongestCreature()).foreach(renderer.processEntity)

      //      renderer.processEntity(entity)
      //      renderer.processEntity(entity2)

      renderer.render(light, camera)
      DisplayManager.updateDisplay()
    }

    renderer.cleanUp()
    loader.cleanUp()
    DisplayManager.closeDisplay()
    //solverThread.stop()
  }


  private def cubeMatrixToEntities(loader: Loader, cubeSolution: Array[Array[Array[Int]]], baseLocation: Vector3f) = {
    val cubes = for {
      x <- 0 until cubeSolution.length
      y <- 0 until cubeSolution.length
      z <- 0 until cubeSolution.length
      if cubeSolution(x)(y)(z) > 0
    } yield {

      val texture = if ((x + y + z) % 2 == 0) "dark-brown" else "light-brown"
      val model = loadToVao("cube", loader)
      val modelTexture = ModelTexture(loader.loadTexture(texture), 10, 0.2f)
      val cubeModel = new TexturedModel(model, modelTexture)
      val location = new Vector3f((2 + 0.01f) * x, (2 + 0.01f) * y, (2 + 0.01f) * z)
      val cubeEntity = Entity(cubeModel, location add baseLocation, 0, 0, 0, 1)
      cubeEntity
    }
    cubes
  }


  private def loadToVao(fileName: String, loader: Loader) = {
    val data = OBJFileLoader.loadOBJ(fileName)
    loader.loadToVAO(data.getVertices, data.getTextureCoords, data.getNormals, data.getIndices)
  }
}
