package engineTester.cube

import examples.cube.{FoldCreature, Runner}
import genetic.Creature

/**
  * Created by Shmuel Blitz on 6/5/2018.
  */
class Solver(generationCallback: FoldCreature => Unit) extends Runnable{
  var result :(Seq[Int], FoldCreature) = null


  override def run(): Unit = {
     result = new Runner(generationCallback).runExample()
  }
}
