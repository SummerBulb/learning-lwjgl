package textures

/**
  * Created by Shmuel Blitz on 6/10/2018.
  */
case class TerrainTexturePack(backgroundTexture: TerrainTexture, rTexture:TerrainTexture,
                              gTexture:TerrainTexture, bTexture:TerrainTexture){

}
