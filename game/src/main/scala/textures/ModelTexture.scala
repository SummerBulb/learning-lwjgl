package textures

/**
  * Created by Shmuel Blitz on 5/13/2018.
  */
case class ModelTexture(id: Int, var shineDamper: Float = 1, var reflectivity: Float = 0,
                        hasTransparency: Boolean = false, useFakeLighting:Boolean = false, val numberOfRows:Int = 1) {

}
