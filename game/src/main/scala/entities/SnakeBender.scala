package entities

import org.joml.Vector3f
import org.lwjgl.glfw.GLFW
import renderEngine.{DisplayManager, KeyboardInput}
import terrains.Terrain

/**
  * Created by Shmuel Blitz on 6/7/2018.
  */
class SnakeBender(val entity: Entity, var index: Int, kbInput: KeyboardInput, collection: Seq[Entity]) extends Player {

  var lastSeclectorMove = DisplayManager.getCurrentTime()

  def setPosition = {
    val cubePos = collection(index).position
    entity.position = new Vector3f(cubePos.x, cubePos.y + 1.5f, cubePos.z)
    lastSeclectorMove = DisplayManager.getCurrentTime()
    println(lastSeclectorMove)
  }

  def moveSelector() = {

    val movesPerSecond = 10
    if ((DisplayManager.getCurrentTime() - lastSeclectorMove) * movesPerSecond > 1000) {
      if (kbInput.keys(GLFW.GLFW_KEY_RIGHT_BRACKET)) {
        index += 1
        if (index >= collection.length)
          index = collection.length - 1
        setPosition

      }
      if (kbInput.keys(GLFW.GLFW_KEY_LEFT_BRACKET)) {
        index -= 1
        if (index < 0)
          index = 0
        setPosition
      }
    }

  }

  var U_released = true
  var J_released = true

  var spaceReleased = true

  def moveSelected() = {
    if (U_released) {
      if (kbInput.keys(GLFW.GLFW_KEY_U)) {
        U_released = false
        collection(index).position.y += 2
        setPosition
      }
    }
    if (!kbInput.keys(GLFW.GLFW_KEY_U)) {
      U_released = true
    }

    if (J_released) {
      if (kbInput.keys(GLFW.GLFW_KEY_J)) {
        J_released = false
        collection(index).position.y -= 2
        setPosition
      }
    }
    if (!kbInput.keys(GLFW.GLFW_KEY_J)) {
      J_released = true
    }

//    if (spaceReleased) {
//      if (kbInput.keys(GLFW.GLFW_KEY_SPACE)) {
//        spaceReleased = false
//
//        if (index == 0) {
//          collection.drop(index + 1) foreach { item => item.position.rotateY(Math.toRadians(90).toFloat) }
//        }
//        else {
//          val rotationPoint = collection(index).position
//          collection.drop(index + 1).foreach(
//            item => {
//              item.position.sub(rotationPoint)
//              item.position.rotateZ(Math.toRadians(90).toFloat)
//              item.position.add(rotationPoint)
//            }
//          )
//
//        }
//
//      }
//    }

    if (!kbInput.keys(GLFW.GLFW_KEY_SPACE)) {
      spaceReleased = true
    }

  }


  override def move(terrain: Terrain): Unit = moveSelected

  override def entities(): Seq[Entity] = Seq(entity)
}
