package entities

/**
  * Created by Shmuel Blitz on 6/17/2018.
  */
trait Controlable {

  def move()

}
