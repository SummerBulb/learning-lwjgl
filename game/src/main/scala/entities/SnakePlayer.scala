package entities

import guis.GuiTexture
import models.TexturedModel
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW
import renderEngine.DisplayManager
import terrains.Terrain

/**
  * Created by Shmuel Blitz on 6/11/2018.
  */
class SnakePlayer(models: Seq[TexturedModel], position: Vector3f, rotX: Float, rotY: Float, rotZ: Float, scale: Float,
                  powerBar: GuiTexture, powerups: Seq[Entity])
  extends Player {

  var snake = generateSnakeEntities(8, position)

  private def generateSnakeEntities(length:Int, location: Vector3f) = {
    (0 until length).map(i => {
      val pos = new Vector3f(location)
      .add(new Vector3f(0, 0, 2 * -i))
      Entity(models(i % 2), pos, rotX, rotY, rotZ, scale)
    })
  }

  var isAlive = true

  val RUN_SPEED = 20f
  val TURN_SPEED = 160
  val GRAVITY = -50
  val JUMP_POWER = 30

  var currentSpeed = RUN_SPEED
  var currentTurnSpeed = 0f
  var upwardsSpeed = 0f

  val powerPerUnit = 0.0005f
  val fullPower = powerBar.scale.x
  val powerBarXPos = powerBar.position.x

  var isInAir = false


  override def entities(): Seq[Entity] = {
    snake
  }

  def move(terrain: Terrain) = {
    checkInputs()

    if (isAlive) {
      snake.head.increaseRotation(0, currentTurnSpeed * DisplayManager.frameTimeSeconds, 0)
      val distance = currentSpeed * DisplayManager.frameTimeSeconds


      val powerUsed = Math.abs(distance) * powerPerUnit
      powerBar.scale.x -= powerUsed
      powerBar.position.x -= powerUsed


      val rotY = snake.head.rotY
      val dx = (distance * Math.sin(Math.toRadians(rotY))).toFloat
      val dz = (distance * Math.cos(Math.toRadians(rotY))).toFloat
      upwardsSpeed += GRAVITY * DisplayManager.frameTimeSeconds
      snake.head.increasePosition(dx, upwardsSpeed * DisplayManager.frameTimeSeconds, dz)

      val terrainHeight = terrain.getHeightOfTerrain(snake.head.position.x, snake.head.position.z)

      if (snake.head.position.y < terrainHeight) {
        upwardsSpeed = 0
        isInAir = false
        snake.head.position.y = terrainHeight
      }

      moveTail(terrain)

      if (shouldPowerUp()) recharge()

      if (testCollision())  {
        isAlive = false
      }
    }
  }

  private def moveTail(terrain: Terrain) = {
    if (currentSpeed != 0) {
      for (i <- 1 until snake.size) {
        /* val destination = new Vector3f(snake(i - 1).position)
         destination.sub(snake(i).position).normalize().mul(distance)
         snake(i).position.add(destination)*/

        val newPos = new Vector3f(snake(i).position)
          .sub(snake(i - 1).position)
          .normalize(1.5f)
          .add(snake(i - 1).position)
        val terrainHeight = terrain.getHeightOfTerrain(newPos.x, newPos.z)
        if (newPos.y < terrainHeight) {
          newPos.y = terrainHeight
        }
        snake(i).position = newPos
        snake(i).rotX = snake(i - 1).rotX
        snake(i).rotY = snake(i - 1).rotY
        snake(i).rotZ = snake(i - 1).rotZ
      }
    } else {
      for (i <- Range.inclusive(snake.size - 1, 1, -1)) {
        val pos = snake(i).position
        pos.y = snake(i - 1).position.y
        val terrainHeight = terrain.getHeightOfTerrain(pos.x, pos.z)
        if (pos.y < terrainHeight) {
          pos.y = terrainHeight
        }
        snake(i).rotX = snake(i - 1).rotX
        snake(i).rotY = snake(i - 1).rotY
        snake(i).rotZ = snake(i - 1).rotZ
      }
    }
  }

  private def jump = {
    if (!isInAir) {
      isInAir = true
      upwardsSpeed = JUMP_POWER
    }
  }

  def recharge() = {
    powerBar.position.x = powerBarXPos
    powerBar.scale.x = fullPower

    snake = snake ++ generateSnakeEntities(2, snake.last.position)
    currentSpeed += 0.5f
  }

  def shouldPowerUp() = {
    powerups.exists(p => {

      val distnace = Math.pow(p.position.x - snake.head.position.x, 2) + Math.pow(p.position.y - snake.head.position.y, 2)

      distnace < 1

    })
  }


  def checkInputs() = {
    if (powerBar.scale.x > 0) {
      if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_W)) {
        currentSpeed = RUN_SPEED
      } else if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_S)) {
        //currentSpeed = -RUN_SPEED
      } else {
        //currentSpeed = 0
      }
    } else {
      //currentSpeed = 0
    }

    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_T)) {
      currentSpeed = 0
    }


    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_D)) {
      currentTurnSpeed = -TURN_SPEED
    } else if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_A)) {
      currentTurnSpeed = TURN_SPEED
    } else {
      currentTurnSpeed = 0
    }

    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_SPACE)) {
      jump
    }

    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_P)) {
      recharge()
    }

    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_K)) {
      isAlive = false
    }

  }

  def testCollision() = {
    snake.tail.tail.exists(tailPart => {
      val partPosition = new Vector3f(tailPart.position)
      partPosition.sub(snake.head.position)

      Math.abs(partPosition.length()) < 2f

    })
  }


}
