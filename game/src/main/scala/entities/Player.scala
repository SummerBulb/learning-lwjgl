package entities

import terrains.Terrain

/**
  * Created by Shmuel Blitz on 6/21/2018.
  */
trait Player {
  def move(terrain:Terrain):Unit
  def entities():Seq[Entity]
}
