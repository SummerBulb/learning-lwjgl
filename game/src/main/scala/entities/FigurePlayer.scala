package entities

import guis.GuiTexture
import javax.print.DocFlavor.INPUT_STREAM
import models.TexturedModel
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW
import renderEngine.{DisplayManager, KeyboardInput}
import terrains.Terrain

/**
  * Created by Shmuel Blitz on 6/11/2018.
  */
class FigurePlayer(_model: TexturedModel, _position: Vector3f, _rotX: Float, _rotY: Float, _rotZ: Float, scale: Float,
                   powerBar: GuiTexture, powerups: Seq[Entity])
  extends Entity(_model, _position, _rotX, _rotY, _rotZ, scale) with Player {

  val RUN_SPEED = 20
  val TURN_SPEED = 160
  val GRAVITY = -50
  val JUMP_POWER = 30

  var currentSpeed = 0f
  var currentTurnSpeed = 0f
  var upwardsSpeed = 0f

  val powerPerUnit = 0.0005f
  val fullPower = powerBar.scale.x
  val powerBarXPos = powerBar.position.x

  var isInAir = false


  override def entities(): List[Entity] = {
    this :: Nil
  }

  def move(terrain: Terrain) = {
    checkInputs()
    super.increaseRotation(0, currentTurnSpeed * DisplayManager.frameTimeSeconds, 0)
    val distance = currentSpeed * DisplayManager.frameTimeSeconds
    val powerUsed = Math.abs(distance) * powerPerUnit
    powerBar.scale.x -= powerUsed
    powerBar.position.x -= powerUsed
    val dx = (distance * Math.sin(Math.toRadians(rotY))).toFloat
    val dz = (distance * Math.cos(Math.toRadians(rotY))).toFloat
    upwardsSpeed += GRAVITY * DisplayManager.frameTimeSeconds
    super.increasePosition(dx, upwardsSpeed * DisplayManager.frameTimeSeconds, dz)

    val terrainHeight = terrain.getHeightOfTerrain(position.x, position.z)

    if (position.y < terrainHeight) {
      upwardsSpeed = 0
      isInAir = false
      position.y = terrainHeight
    }

    if (shouldPowerUp()) recharge()
  }

  private def jump = {
    if (!isInAir) {
      isInAir = true
      upwardsSpeed = JUMP_POWER
    }
  }

  def recharge() = {
    powerBar.position.x = powerBarXPos
    powerBar.scale.x = fullPower
  }

  def shouldPowerUp() = {
    powerups.exists(p => {

      val distnace = Math.pow(p.position.x - this.position.x, 2) + Math.pow(p.position.y - this.position.y, 2)

      distnace < 1

    })
  }


  def checkInputs() = {
    if (powerBar.scale.x > 0) {
      if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_W)) {
        currentSpeed = RUN_SPEED
      } else if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_S)) {
        currentSpeed = -RUN_SPEED
      } else {
        currentSpeed = 0
      }
    } else {
      currentSpeed = 0
    }


    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_D)) {
      currentTurnSpeed = -TURN_SPEED
    } else if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_A)) {
      currentTurnSpeed = TURN_SPEED
    } else {
      currentTurnSpeed = 0
    }

    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_SPACE)) {
      jump
    }

    if (DisplayManager.kbInput.keys(GLFW.GLFW_KEY_P)) {
      recharge()
    }

  }


}
