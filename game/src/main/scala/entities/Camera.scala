package entities

import org.joml.Vector3f
import org.lwjgl.glfw.GLFW
import renderEngine.{DisplayManager, KeyboardInput}
import terrains.Terrain

/**
  * Created by Shmuel Blitz on 5/16/2018.
  */
class Camera(player: Player, terrain: Terrain) {

  var distanceFromPlayer = 50f
  var angleAroundPlayer = 0f

  val position = new Vector3f(0, 0, 0)
  var pitch = 10f
  var yaw = 180f
  var roll = 0f

  val moveSpeed = 0.2f
  val pitchAngle = 1.2f

  def move(): Unit = {
    calculateZoom()
    calculatePitch()
    calculateAngleAroundPlayer()
    val horizontalDistance = calculateHorizontalDistance()
    val verticalDistance = calculateVerticalDistance()
    calculateCameraPosition(horizontalDistance, verticalDistance)
    yaw = 180 - (player.entities.head.rotY + angleAroundPlayer)
  }

  def calculateCameraPosition(horizDistance: Float, verticDistance: Float) = {
    val theta = player.entities.head.rotY + angleAroundPlayer
    val xOffset = (horizDistance * Math.sin(Math.toRadians(theta))).toFloat
    val zOffset = (horizDistance * Math.cos(Math.toRadians(theta))).toFloat
    val playerPos = player.entities.head.position
    position.x = playerPos.x - xOffset
    position.y = playerPos.y + verticDistance
    position.z = playerPos.z - zOffset

    if (terrain != null) {
      val terrainHeight = terrain.getHeightOfTerrain(position.x, position.z)

      if (position.y < terrainHeight) {
        position.y = terrainHeight + 1
      }
    }
  }

  def calculateZoom() = {
    val zoomLevel = DisplayManager.mouseScrollInput.yOffset //* 0.1f

    distanceFromPlayer -= zoomLevel
  }

  def calculatePitch() = {
    if (DisplayManager.mouseButtonInput.buttons(1)) {
      val pitchChange = DisplayManager.cursorPositionInput.dy * 0.1f
      pitch += pitchChange
      if (pitch < 2f) {
        pitch = 2
      }
      if (pitch > 90f) {
        pitch = 90
      }

    }
  }

  def calculateAngleAroundPlayer(): Unit = {
    if (DisplayManager.mouseButtonInput.buttons(0)) {
      val angleChange = DisplayManager.cursorPositionInput.dx * 0.3f
      angleAroundPlayer -= angleChange
    }
  }

  def calculateHorizontalDistance() = {
    (distanceFromPlayer * Math.cos(Math.toRadians(pitch))).toFloat
  }

  def calculateVerticalDistance() = {
    (distanceFromPlayer * Math.sin(Math.toRadians(pitch))).toFloat
  }

  private def printPosition = {
    if (false)
      println(this)
  }

  override def toString = s"Camera($position, $pitch, $yaw, $roll)"
}
