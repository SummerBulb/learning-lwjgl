package entities

import models.TexturedModel
import org.joml.Vector3f

/**
  * Created by Shmuel Blitz on 5/16/2018.
  */
case class Entity(val model: TexturedModel, var position: Vector3f, var rotX: Float, var rotY: Float, var rotZ: Float,
                  var scale: Float, val textureIndex: Int = 0) {

  def getTextureXOffset() = {
    val column = textureIndex % model.texture.numberOfRows
    column.toFloat / model.texture.numberOfRows.toFloat
  }

  def getTextureYOffset() = {
    val row = textureIndex / model.texture.numberOfRows
    row.toFloat / model.texture.numberOfRows.toFloat
  }

    def increasePosition(dx: Float, dy: Float, dz: Float) = {
    position.x += dx
    position.y += dy
    position.z += dz
  }

  def increaseRotation(dx: Float, dy: Float, dz: Float) = {
    rotX += dx
    rotY += dy
    rotZ += dz
  }
}
